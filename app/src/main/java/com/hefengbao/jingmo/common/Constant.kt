package com.hefengbao.jingmo.common

object Constant {
    val DB_NAME = "jingmo.db"
    val DATASTORE_NAME = "jingmo"
    val DATASTORE_DATASET_NAME = "jingmo_dataset"
    val DATASTORE_READ_STATUS_NAME = "jingmo_read_status"
}